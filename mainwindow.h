#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QtMultimedia/QMediaPlayer>
#include <QMessageBox>
#include "wavesound.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    WaveSound ws;
    QAudioBuffer::S16S *Frames;
    QMediaPlayer *MediaPlayer;
    QString AudioInFile;
    QString AudioOutFile;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_actionExit_triggered();

    void on_actionAbout_triggered();

    void on_Filter_Box_currentIndexChanged(int index);

    void on_Play_Audio_In_clicked();

    void on_Play_Audio_Out_clicked();

    void on_Search_Audio_In_clicked();

    void on_Search_Audio_Out_clicked();

    void on_Stop_Audio_In_clicked();

    void on_Stop_Audio_Out_clicked();

    void on_run_filter_clicked();

    void on_Action_Output_Audio_File_triggered();

    void on_Audio_Out_currentIndexChanged(const QString &arg1);

    void on_Action_Input_Audio_File_triggered();

    void on_Audio_In_currentIndexChanged(const QString &arg1);

private:
    void setFilesAndButtonStatus(const QString &fileName);

    Ui::MainWindow *ui;

};

#endif //MAINWINDOW_H
