#ifndef AUDIOMANIP_H
#define AUDIOMANIP_H

#include <QAudioBuffer>

///\file

///
// Define to make the name of the object easier for the student.
/// \~English
/// AudioBuffer is an object with left and right side of an stereo audio
/// \~Spanish
/// AudioBuffer es un objecte con el lado izquierdo y derecho de un audio estero.
///
#define AudioBuffer QAudioBuffer::S16S


// Prototypes for the functions to be implemented in the lab
/// \fn void RemoveVocals(AudioBuffer frames[], int N)
/// \~English
/// \brief Given the audio frames of the sound clip, remove
///                      the vocals from the clip.
/// \param frames array that contains audio samples, it will be modified
///                this function.
/// \param N number of audio samples (per channel)
/// \~Spanish
/// \brief Dadas las tramas de audio de un clip de sonido, remueve
///         las vocales del clip
/// \param arreglo de tramas que contiene las muestras, será modificado en
/// esta funcion.
/// \param N numero de muestras de audio (por canal)
void RemoveVocals(AudioBuffer frames[], int N);

/// \fn void AudioFadeIn(AudioBuffer frames[], int N, int fade_length)
/// \~English
/// \brief Perform an audio fade in of the sound clip
/// \param frames array that contains audio samples, it will modified
///                this function.
/// \param N number of audio samples (per channel)
/// \param fade_length length of the fade in, in terms of audio samples
/// \~Spanish
/// \brief Performa un desbanecimiento de audio de un clip de sonido
/// \param arreglo de tramas que contiene las muestras, será modificado en
/// esta funcion.
/// \param N numero de muestras de audio (por canal)
/// \param fade_length largo del desbanecimiento, en terminos de muestras de
/// audio.
///
void AudioFadeIn (AudioBuffer frames[], int N, int fade_length);

/// \fn void AudioFadeOut(AudioBuffer frames[], int N, int fade_length)
/// \~English
/// \brief Perform an audio fade out of the sound clip
/// \param frames array that contains audio samples, it will modified
///                this function.
/// \param N number of audio samples (per channel)
/// \param fade_length length of the fade out, in terms of audio samples
///
/// \~Spanish
/// \brief Performa un incremento del sonido de audio de un clip de sonido
/// \param arreglo de tramas que contiene las muestras, será modificado en
/// esta funcion.
/// \param N numero de muestras de audio (por canal)
/// \param fade_length largo del incremento del sonido, en terminos de muestras de
/// audio.
///
void AudioFadeOut(AudioBuffer frames[], int N, int fade_length);

/// \fn void LeftToRight(AudioBuffer frames[], int N, int pan_length)
/// \~English
/// \brief Performs a panning effect from left to right
/// \param frames array that contains audio samples, it will modified
///                this function.
/// \param N number of audio samples (per channel)
/// \param pan_length length of the panning effect, in terms of audio samples
///
/// \~Spanish
/// \brief Performa un efecto panoramico del audio de un clip de sonido
/// \param arreglo de tramas que contiene las muestras, será modificado en
/// esta funcion.
/// \param N numero de muestras de audio (por canal)
/// \param pan_length largo del efecto panoramico, en terminos de muestras de
/// audio.
void LeftToRight (AudioBuffer frames[], int N, int pan_length);

#endif
